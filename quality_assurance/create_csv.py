import pandas as pd
import numpy as np

filename = 'proba_model_golf_wdw.txt'
fileout = 'golf_wdw_quality_assurance.csv'

a  = np.loadtxt (filename)
columns = ['n', 'l', 'psw', 'ps', 'p0', 'n_model']

df = pd.DataFrame (data=a, columns=columns)
df = df.set_index (['n', 'l'])

df['ln_K_sw'] = np.log (df['psw']) - np.log (df['p0']+df['ps'])
df['ln_K_s'] = np.log (df['psw']+df['ps']) - np.log (df['p0'])

df.to_csv (fileout)
